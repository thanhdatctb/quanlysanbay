/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client.Model;

import Client.View.Main;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanh
 */
public class MyConstance {
    public static Server.IQuanLySanBay ql;
    public MyConstance(){
        
        try {
            // TODO add your handling code here:
            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 1099);
            reg.lookup("qlsb");
            ql = (Server.IQuanLySanBay) reg.lookup("qlsb");
            
        } catch (RemoteException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
