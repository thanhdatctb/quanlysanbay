/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;


import Server.Controller.ChuyenBayController;
import Server.Controller.MayBayController;
import Server.Model.Tblmaybay;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.rmi.RMIServer;

/**
 *
 * @author thanh
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            
            Registry reg = LocateRegistry.createRegistry(1099);
            QuanLySanBayImplement lg = new QuanLySanBayImplement();
            reg.rebind("qlsb", lg);            
            System.out.println("Server is ready");
            //System.out.println(lg.LogIn("admin", "admin"));
            System.out.println(new ChuyenBayController().GetAll().size());
            
        } catch (RemoteException ex) {
            Logger.getLogger(RMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
