/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblnv_sdt")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblnvSdt.findAll", query = "SELECT t FROM TblnvSdt t")
    , @NamedQuery(name = "TblnvSdt.findBySMaNV", query = "SELECT t FROM TblnvSdt t WHERE t.tblnvSdtPK.sMaNV = :sMaNV")
    , @NamedQuery(name = "TblnvSdt.findBySSDT", query = "SELECT t FROM TblnvSdt t WHERE t.tblnvSdtPK.sSDT = :sSDT")})
public class TblnvSdt implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TblnvSdtPK tblnvSdtPK;
    @JoinColumn(name = "sMaNV", referencedColumnName = "sMaNV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tblnhanvien tblnhanvien;

    public TblnvSdt() {
    }

    public TblnvSdt(TblnvSdtPK tblnvSdtPK) {
        this.tblnvSdtPK = tblnvSdtPK;
    }

    public TblnvSdt(String sMaNV, String sSDT) {
        this.tblnvSdtPK = new TblnvSdtPK(sMaNV, sSDT);
    }

    public TblnvSdtPK getTblnvSdtPK() {
        return tblnvSdtPK;
    }

    public void setTblnvSdtPK(TblnvSdtPK tblnvSdtPK) {
        this.tblnvSdtPK = tblnvSdtPK;
    }

    public Tblnhanvien getTblnhanvien() {
        return tblnhanvien;
    }

    public void setTblnhanvien(Tblnhanvien tblnhanvien) {
        this.tblnhanvien = tblnhanvien;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tblnvSdtPK != null ? tblnvSdtPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblnvSdt)) {
            return false;
        }
        TblnvSdt other = (TblnvSdt) object;
        if ((this.tblnvSdtPK == null && other.tblnvSdtPK != null) || (this.tblnvSdtPK != null && !this.tblnvSdtPK.equals(other.tblnvSdtPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.TblnvSdt[ tblnvSdtPK=" + tblnvSdtPK + " ]";
    }
    
}
