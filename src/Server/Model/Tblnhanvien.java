/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblnhanvien")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tblnhanvien.findAll", query = "SELECT t FROM Tblnhanvien t")
    , @NamedQuery(name = "Tblnhanvien.findBySMaNV", query = "SELECT t FROM Tblnhanvien t WHERE t.sMaNV = :sMaNV")
    , @NamedQuery(name = "Tblnhanvien.findBySHoten", query = "SELECT t FROM Tblnhanvien t WHERE t.sHoten = :sHoten")
    , @NamedQuery(name = "Tblnhanvien.findBySGT", query = "SELECT t FROM Tblnhanvien t WHERE t.sGT = :sGT")
    , @NamedQuery(name = "Tblnhanvien.findBySDC", query = "SELECT t FROM Tblnhanvien t WHERE t.sDC = :sDC")
    , @NamedQuery(name = "Tblnhanvien.findBySVitri", query = "SELECT t FROM Tblnhanvien t WHERE t.sVitri = :sVitri")
    , @NamedQuery(name = "Tblnhanvien.findBySHsl", query = "SELECT t FROM Tblnhanvien t WHERE t.sHsl = :sHsl")
    , @NamedQuery(name = "Tblnhanvien.findByTuoi", query = "SELECT t FROM Tblnhanvien t WHERE t.tuoi = :tuoi")})
public class Tblnhanvien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sMaNV")
    private String sMaNV;
    @Column(name = "sHoten")
    private String sHoten;
    @Column(name = "sGT")
    private String sGT;
    @Column(name = "sDC")
    private String sDC;
    @Column(name = "sVitri")
    private String sVitri;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sHsl")
    private Double sHsl;
    @Column(name = "tuoi")
    private Integer tuoi;
    @ManyToMany(mappedBy = "tblnhanvienCollection")
    private Collection<Tblchuyenbay> tblchuyenbayCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblnhanvien")
    private Collection<TblnvSdt> tblnvSdtCollection;

    public Tblnhanvien() {
    }

    public Tblnhanvien(String sMaNV) {
        this.sMaNV = sMaNV;
    }

    public String getSMaNV() {
        return sMaNV;
    }

    public void setSMaNV(String sMaNV) {
        this.sMaNV = sMaNV;
    }

    public String getSHoten() {
        return sHoten;
    }

    public void setSHoten(String sHoten) {
        this.sHoten = sHoten;
    }

    public String getSGT() {
        return sGT;
    }

    public void setSGT(String sGT) {
        this.sGT = sGT;
    }

    public String getSDC() {
        return sDC;
    }

    public void setSDC(String sDC) {
        this.sDC = sDC;
    }

    public String getSVitri() {
        return sVitri;
    }

    public void setSVitri(String sVitri) {
        this.sVitri = sVitri;
    }

    public Double getSHsl() {
        return sHsl;
    }

    public void setSHsl(Double sHsl) {
        this.sHsl = sHsl;
    }

    public Integer getTuoi() {
        return tuoi;
    }

    public void setTuoi(Integer tuoi) {
        this.tuoi = tuoi;
    }

    @XmlTransient
    public Collection<Tblchuyenbay> getTblchuyenbayCollection() {
        return tblchuyenbayCollection;
    }

    public void setTblchuyenbayCollection(Collection<Tblchuyenbay> tblchuyenbayCollection) {
        this.tblchuyenbayCollection = tblchuyenbayCollection;
    }

    @XmlTransient
    public Collection<TblnvSdt> getTblnvSdtCollection() {
        return tblnvSdtCollection;
    }

    public void setTblnvSdtCollection(Collection<TblnvSdt> tblnvSdtCollection) {
        this.tblnvSdtCollection = tblnvSdtCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaNV != null ? sMaNV.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblnhanvien)) {
            return false;
        }
        Tblnhanvien other = (Tblnhanvien) object;
        if ((this.sMaNV == null && other.sMaNV != null) || (this.sMaNV != null && !this.sMaNV.equals(other.sMaNV))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.Tblnhanvien[ sMaNV=" + sMaNV + " ]";
    }
    
}
