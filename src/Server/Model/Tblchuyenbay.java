/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblchuyenbay")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tblchuyenbay.findAll", query = "SELECT t FROM Tblchuyenbay t")
    , @NamedQuery(name = "Tblchuyenbay.findBySMaCB", query = "SELECT t FROM Tblchuyenbay t WHERE t.sMaCB = :sMaCB")
    , @NamedQuery(name = "Tblchuyenbay.findBySTenCB", query = "SELECT t FROM Tblchuyenbay t WHERE t.sTenCB = :sTenCB")
    , @NamedQuery(name = "Tblchuyenbay.findByDNgayKH", query = "SELECT t FROM Tblchuyenbay t WHERE t.dNgayKH = :dNgayKH")
    , @NamedQuery(name = "Tblchuyenbay.findBySGioBD", query = "SELECT t FROM Tblchuyenbay t WHERE t.sGioBD = :sGioBD")
    , @NamedQuery(name = "Tblchuyenbay.findBySGioKT", query = "SELECT t FROM Tblchuyenbay t WHERE t.sGioKT = :sGioKT")
    , @NamedQuery(name = "Tblchuyenbay.findByFMaxHL", query = "SELECT t FROM Tblchuyenbay t WHERE t.fMaxHL = :fMaxHL")})
public class Tblchuyenbay implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sMaCB")
    private String sMaCB;
    @Column(name = "sTenCB")
    private String sTenCB;
    @Column(name = "dNgayKH")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dNgayKH;
    @Column(name = "sGioBD")
    private String sGioBD;
    @Column(name = "sGioKT")
    private String sGioKT;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fMaxHL")
    private Double fMaxHL;
    @JoinTable(name = "tblnv_cb", joinColumns = {
        @JoinColumn(name = "sMaCB", referencedColumnName = "sMaCB")}, inverseJoinColumns = {
        @JoinColumn(name = "sMaNV", referencedColumnName = "sMaNV")})
    @ManyToMany
    private Collection<Tblnhanvien> tblnhanvienCollection;
    @JoinColumn(name = "sMaLT", referencedColumnName = "sMaLT")
    @ManyToOne
    private Tbllotrinh sMaLT;
    @JoinColumn(name = "sMaMB", referencedColumnName = "sMaMB")
    @ManyToOne
    private Tblmaybay sMaMB;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblchuyenbay")
    private Collection<TblkhCb> tblkhCbCollection;

    public Tblchuyenbay() {
    }

    public Tblchuyenbay(String sMaCB) {
        this.sMaCB = sMaCB;
    }

    public String getSMaCB() {
        return sMaCB;
    }

    public void setSMaCB(String sMaCB) {
        this.sMaCB = sMaCB;
    }

    public String getSTenCB() {
        return sTenCB;
    }

    public void setSTenCB(String sTenCB) {
        this.sTenCB = sTenCB;
    }

    public Date getDNgayKH() {
        return dNgayKH;
    }

    public void setDNgayKH(Date dNgayKH) {
        this.dNgayKH = dNgayKH;
    }

    public String getSGioBD() {
        return sGioBD;
    }

    public void setSGioBD(String sGioBD) {
        this.sGioBD = sGioBD;
    }

    public String getSGioKT() {
        return sGioKT;
    }

    public void setSGioKT(String sGioKT) {
        this.sGioKT = sGioKT;
    }

    public Double getFMaxHL() {
        return fMaxHL;
    }

    public void setFMaxHL(Double fMaxHL) {
        this.fMaxHL = fMaxHL;
    }

    @XmlTransient
    public Collection<Tblnhanvien> getTblnhanvienCollection() {
        return tblnhanvienCollection;
    }

    public void setTblnhanvienCollection(Collection<Tblnhanvien> tblnhanvienCollection) {
        this.tblnhanvienCollection = tblnhanvienCollection;
    }

    public Tbllotrinh getSMaLT() {
        return sMaLT;
    }

    public void setSMaLT(Tbllotrinh sMaLT) {
        this.sMaLT = sMaLT;
    }

    public Tblmaybay getSMaMB() {
        return sMaMB;
    }

    public void setSMaMB(Tblmaybay sMaMB) {
        this.sMaMB = sMaMB;
    }

    @XmlTransient
    public Collection<TblkhCb> getTblkhCbCollection() {
        return tblkhCbCollection;
    }

    public void setTblkhCbCollection(Collection<TblkhCb> tblkhCbCollection) {
        this.tblkhCbCollection = tblkhCbCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaCB != null ? sMaCB.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblchuyenbay)) {
            return false;
        }
        Tblchuyenbay other = (Tblchuyenbay) object;
        if ((this.sMaCB == null && other.sMaCB != null) || (this.sMaCB != null && !this.sMaCB.equals(other.sMaCB))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.Tblchuyenbay[ sMaCB=" + sMaCB + " ]";
    }
    
}
