/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tbllotrinh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbllotrinh.findAll", query = "SELECT t FROM Tbllotrinh t")
    , @NamedQuery(name = "Tbllotrinh.findBySMaLT", query = "SELECT t FROM Tbllotrinh t WHERE t.sMaLT = :sMaLT")
    , @NamedQuery(name = "Tbllotrinh.findBySDiemBD", query = "SELECT t FROM Tbllotrinh t WHERE t.sDiemBD = :sDiemBD")
    , @NamedQuery(name = "Tbllotrinh.findBySDiemKT", query = "SELECT t FROM Tbllotrinh t WHERE t.sDiemKT = :sDiemKT")})
public class Tbllotrinh implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sMaLT")
    private String sMaLT;
    @Column(name = "sDiemBD")
    private String sDiemBD;
    @Column(name = "sDiemKT")
    private String sDiemKT;
    @OneToMany(mappedBy = "sMaLT")
    private Collection<Tblchuyenbay> tblchuyenbayCollection;

    public Tbllotrinh() {
    }

    public Tbllotrinh(String sMaLT) {
        this.sMaLT = sMaLT;
    }

    public String getSMaLT() {
        return sMaLT;
    }

    public void setSMaLT(String sMaLT) {
        this.sMaLT = sMaLT;
    }

    public String getSDiemBD() {
        return sDiemBD;
    }

    public void setSDiemBD(String sDiemBD) {
        this.sDiemBD = sDiemBD;
    }

    public String getSDiemKT() {
        return sDiemKT;
    }

    public void setSDiemKT(String sDiemKT) {
        this.sDiemKT = sDiemKT;
    }

    @XmlTransient
    public Collection<Tblchuyenbay> getTblchuyenbayCollection() {
        return tblchuyenbayCollection;
    }

    public void setTblchuyenbayCollection(Collection<Tblchuyenbay> tblchuyenbayCollection) {
        this.tblchuyenbayCollection = tblchuyenbayCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaLT != null ? sMaLT.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbllotrinh)) {
            return false;
        }
        Tbllotrinh other = (Tbllotrinh) object;
        if ((this.sMaLT == null && other.sMaLT != null) || (this.sMaLT != null && !this.sMaLT.equals(other.sMaLT))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.Tbllotrinh[ sMaLT=" + sMaLT + " ]";
    }
    
}
