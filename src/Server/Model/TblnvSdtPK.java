/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author thanh
 */
@Embeddable
public class TblnvSdtPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "sMaNV")
    private String sMaNV;
    @Basic(optional = false)
    @Column(name = "sSDT")
    private String sSDT;

    public TblnvSdtPK() {
    }

    public TblnvSdtPK(String sMaNV, String sSDT) {
        this.sMaNV = sMaNV;
        this.sSDT = sSDT;
    }

    public String getSMaNV() {
        return sMaNV;
    }

    public void setSMaNV(String sMaNV) {
        this.sMaNV = sMaNV;
    }

    public String getSSDT() {
        return sSDT;
    }

    public void setSSDT(String sSDT) {
        this.sSDT = sSDT;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaNV != null ? sMaNV.hashCode() : 0);
        hash += (sSDT != null ? sSDT.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblnvSdtPK)) {
            return false;
        }
        TblnvSdtPK other = (TblnvSdtPK) object;
        if ((this.sMaNV == null && other.sMaNV != null) || (this.sMaNV != null && !this.sMaNV.equals(other.sMaNV))) {
            return false;
        }
        if ((this.sSDT == null && other.sSDT != null) || (this.sSDT != null && !this.sSDT.equals(other.sSDT))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.TblnvSdtPK[ sMaNV=" + sMaNV + ", sSDT=" + sSDT + " ]";
    }
    
}
