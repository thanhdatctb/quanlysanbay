/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author thanh
 */
public class DBService {

    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final String CONN_STRING
            = "jdbc:mysql://localhost/BTL_QuanLySanBay";

    public static Connection getConnection() throws SQLException {
        return (Connection) DriverManager.getConnection(CONN_STRING, USERNAME, PASSWORD);
        
    }
}
