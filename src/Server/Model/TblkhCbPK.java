/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author thanh
 */
@Embeddable
public class TblkhCbPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "sMaKH")
    private String sMaKH;
    @Basic(optional = false)
    @Column(name = "sMaCB")
    private String sMaCB;

    public TblkhCbPK() {
    }

    public TblkhCbPK(String sMaKH, String sMaCB) {
        this.sMaKH = sMaKH;
        this.sMaCB = sMaCB;
    }

    public String getSMaKH() {
        return sMaKH;
    }

    public void setSMaKH(String sMaKH) {
        this.sMaKH = sMaKH;
    }

    public String getSMaCB() {
        return sMaCB;
    }

    public void setSMaCB(String sMaCB) {
        this.sMaCB = sMaCB;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaKH != null ? sMaKH.hashCode() : 0);
        hash += (sMaCB != null ? sMaCB.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblkhCbPK)) {
            return false;
        }
        TblkhCbPK other = (TblkhCbPK) object;
        if ((this.sMaKH == null && other.sMaKH != null) || (this.sMaKH != null && !this.sMaKH.equals(other.sMaKH))) {
            return false;
        }
        if ((this.sMaCB == null && other.sMaCB != null) || (this.sMaCB != null && !this.sMaCB.equals(other.sMaCB))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.TblkhCbPK[ sMaKH=" + sMaKH + ", sMaCB=" + sMaCB + " ]";
    }
    
}
