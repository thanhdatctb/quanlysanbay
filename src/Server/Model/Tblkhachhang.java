/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblkhachhang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tblkhachhang.findAll", query = "SELECT t FROM Tblkhachhang t")
    , @NamedQuery(name = "Tblkhachhang.findBySMaKH", query = "SELECT t FROM Tblkhachhang t WHERE t.sMaKH = :sMaKH")
    , @NamedQuery(name = "Tblkhachhang.findBySHoten", query = "SELECT t FROM Tblkhachhang t WHERE t.sHoten = :sHoten")
    , @NamedQuery(name = "Tblkhachhang.findByITuoi", query = "SELECT t FROM Tblkhachhang t WHERE t.iTuoi = :iTuoi")
    , @NamedQuery(name = "Tblkhachhang.findBySGT", query = "SELECT t FROM Tblkhachhang t WHERE t.sGT = :sGT")
    , @NamedQuery(name = "Tblkhachhang.findByDNgaySinh", query = "SELECT t FROM Tblkhachhang t WHERE t.dNgaySinh = :dNgaySinh")
    , @NamedQuery(name = "Tblkhachhang.findByICMND", query = "SELECT t FROM Tblkhachhang t WHERE t.iCMND = :iCMND")
    , @NamedQuery(name = "Tblkhachhang.findBySDC", query = "SELECT t FROM Tblkhachhang t WHERE t.sDC = :sDC")
    , @NamedQuery(name = "Tblkhachhang.findBySSDT", query = "SELECT t FROM Tblkhachhang t WHERE t.sSDT = :sSDT")})
public class Tblkhachhang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sMaKH")
    private String sMaKH;
    @Column(name = "sHoten")
    private String sHoten;
    @Column(name = "iTuoi")
    private Integer iTuoi;
    @Column(name = "sGT")
    private String sGT;
    @Column(name = "dNgaySinh")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dNgaySinh;
    @Column(name = "iCMND")
    private Integer iCMND;
    @Column(name = "sDC")
    private String sDC;
    @Column(name = "sSDT")
    private String sSDT;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblkhachhang")
    private Collection<TblkhCb> tblkhCbCollection;

    public Tblkhachhang() {
    }

    public Tblkhachhang(String sMaKH) {
        this.sMaKH = sMaKH;
    }

    public String getSMaKH() {
        return sMaKH;
    }

    public void setSMaKH(String sMaKH) {
        this.sMaKH = sMaKH;
    }

    public String getSHoten() {
        return sHoten;
    }

    public void setSHoten(String sHoten) {
        this.sHoten = sHoten;
    }

    public Integer getITuoi() {
        return iTuoi;
    }

    public void setITuoi(Integer iTuoi) {
        this.iTuoi = iTuoi;
    }

    public String getSGT() {
        return sGT;
    }

    public void setSGT(String sGT) {
        this.sGT = sGT;
    }

    public Date getDNgaySinh() {
        return dNgaySinh;
    }

    public void setDNgaySinh(Date dNgaySinh) {
        this.dNgaySinh = dNgaySinh;
    }

    public Integer getICMND() {
        return iCMND;
    }

    public void setICMND(Integer iCMND) {
        this.iCMND = iCMND;
    }

    public String getSDC() {
        return sDC;
    }

    public void setSDC(String sDC) {
        this.sDC = sDC;
    }

    public String getSSDT() {
        return sSDT;
    }

    public void setSSDT(String sSDT) {
        this.sSDT = sSDT;
    }

    @XmlTransient
    public Collection<TblkhCb> getTblkhCbCollection() {
        return tblkhCbCollection;
    }

    public void setTblkhCbCollection(Collection<TblkhCb> tblkhCbCollection) {
        this.tblkhCbCollection = tblkhCbCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaKH != null ? sMaKH.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblkhachhang)) {
            return false;
        }
        Tblkhachhang other = (Tblkhachhang) object;
        if ((this.sMaKH == null && other.sMaKH != null) || (this.sMaKH != null && !this.sMaKH.equals(other.sMaKH))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.Tblkhachhang[ sMaKH=" + sMaKH + " ]";
    }
    
}
