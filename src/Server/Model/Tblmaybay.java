/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblmaybay")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tblmaybay.findAll", query = "SELECT t FROM Tblmaybay t")
    , @NamedQuery(name = "Tblmaybay.findBySMaMB", query = "SELECT t FROM Tblmaybay t WHERE t.sMaMB = :sMaMB")
    , @NamedQuery(name = "Tblmaybay.findBySTenMayBay", query = "SELECT t FROM Tblmaybay t WHERE t.sTenMayBay = :sTenMayBay")
    , @NamedQuery(name = "Tblmaybay.findBySHangHangKhong", query = "SELECT t FROM Tblmaybay t WHERE t.sHangHangKhong = :sHangHangKhong")})
public class Tblmaybay implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sMaMB")
    private String sMaMB;
    @Column(name = "sTenMayBay")
    private String sTenMayBay;
    @Column(name = "sHangHangKhong")
    private String sHangHangKhong;
    @OneToMany(mappedBy = "sMaMB")
    private Collection<Tblchuyenbay> tblchuyenbayCollection;

    public Tblmaybay() {
    }

    public Tblmaybay(String sMaMB) {
        this.sMaMB = sMaMB;
    }

    public String getSMaMB() {
        return sMaMB;
    }

    public void setSMaMB(String sMaMB) {
        this.sMaMB = sMaMB;
    }

    public String getSTenMayBay() {
        return sTenMayBay;
    }

    public void setSTenMayBay(String sTenMayBay) {
        this.sTenMayBay = sTenMayBay;
    }

    public String getSHangHangKhong() {
        return sHangHangKhong;
    }

    public void setSHangHangKhong(String sHangHangKhong) {
        this.sHangHangKhong = sHangHangKhong;
    }

    @XmlTransient
    public Collection<Tblchuyenbay> getTblchuyenbayCollection() {
        return tblchuyenbayCollection;
    }

    public void setTblchuyenbayCollection(Collection<Tblchuyenbay> tblchuyenbayCollection) {
        this.tblchuyenbayCollection = tblchuyenbayCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sMaMB != null ? sMaMB.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tblmaybay)) {
            return false;
        }
        Tblmaybay other = (Tblmaybay) object;
        if ((this.sMaMB == null && other.sMaMB != null) || (this.sMaMB != null && !this.sMaMB.equals(other.sMaMB))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.Tblmaybay[ sMaMB=" + sMaMB + " ]";
    }
    
}
