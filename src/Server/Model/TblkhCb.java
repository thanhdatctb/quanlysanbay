/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thanh
 */
@Entity
@Table(name = "tblkh_cb")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblkhCb.findAll", query = "SELECT t FROM TblkhCb t")
    , @NamedQuery(name = "TblkhCb.findBySMaKH", query = "SELECT t FROM TblkhCb t WHERE t.tblkhCbPK.sMaKH = :sMaKH")
    , @NamedQuery(name = "TblkhCb.findBySMaCB", query = "SELECT t FROM TblkhCb t WHERE t.tblkhCbPK.sMaCB = :sMaCB")
    , @NamedQuery(name = "TblkhCb.findByFBuyHL", query = "SELECT t FROM TblkhCb t WHERE t.fBuyHL = :fBuyHL")
    , @NamedQuery(name = "TblkhCb.findBySLoaiVe", query = "SELECT t FROM TblkhCb t WHERE t.sLoaiVe = :sLoaiVe")
    , @NamedQuery(name = "TblkhCb.findByFGiaVe", query = "SELECT t FROM TblkhCb t WHERE t.fGiaVe = :fGiaVe")})
public class TblkhCb implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TblkhCbPK tblkhCbPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fBuyHL")
    private Double fBuyHL;
    @Column(name = "sLoaiVe")
    private String sLoaiVe;
    @Column(name = "fGiaVe")
    private Double fGiaVe;
    @JoinColumn(name = "sMaKH", referencedColumnName = "sMaKH", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tblkhachhang tblkhachhang;
    @JoinColumn(name = "sMaCB", referencedColumnName = "sMaCB", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tblchuyenbay tblchuyenbay;

    public TblkhCb() {
    }

    public TblkhCb(TblkhCbPK tblkhCbPK) {
        this.tblkhCbPK = tblkhCbPK;
    }

    public TblkhCb(String sMaKH, String sMaCB) {
        this.tblkhCbPK = new TblkhCbPK(sMaKH, sMaCB);
    }

    public TblkhCbPK getTblkhCbPK() {
        return tblkhCbPK;
    }

    public void setTblkhCbPK(TblkhCbPK tblkhCbPK) {
        this.tblkhCbPK = tblkhCbPK;
    }

    public Double getFBuyHL() {
        return fBuyHL;
    }

    public void setFBuyHL(Double fBuyHL) {
        this.fBuyHL = fBuyHL;
    }

    public String getSLoaiVe() {
        return sLoaiVe;
    }

    public void setSLoaiVe(String sLoaiVe) {
        this.sLoaiVe = sLoaiVe;
    }

    public Double getFGiaVe() {
        return fGiaVe;
    }

    public void setFGiaVe(Double fGiaVe) {
        this.fGiaVe = fGiaVe;
    }

    public Tblkhachhang getTblkhachhang() {
        return tblkhachhang;
    }

    public void setTblkhachhang(Tblkhachhang tblkhachhang) {
        this.tblkhachhang = tblkhachhang;
    }

    public Tblchuyenbay getTblchuyenbay() {
        return tblchuyenbay;
    }

    public void setTblchuyenbay(Tblchuyenbay tblchuyenbay) {
        this.tblchuyenbay = tblchuyenbay;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tblkhCbPK != null ? tblkhCbPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblkhCb)) {
            return false;
        }
        TblkhCb other = (TblkhCb) object;
        if ((this.tblkhCbPK == null && other.tblkhCbPK != null) || (this.tblkhCbPK != null && !this.tblkhCbPK.equals(other.tblkhCbPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Server.Model.TblkhCb[ tblkhCbPK=" + tblkhCbPK + " ]";
    }
    
}
