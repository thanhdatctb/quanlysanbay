/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;


import Server.Model.Tblchuyenbay;
import Server.Model.Tblkhachhang;
import Server.Model.Tblmaybay;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author thanh
 */
public interface IQuanLySanBay extends Remote {
    public boolean LogIn(String username, String password) throws RemoteException;
    public List<Tblmaybay> GetAllMayBay() throws RemoteException;
    public List<Tblkhachhang> GetAllKhachHang() throws RemoteException;
    public boolean AddMayBay(Tblmaybay mb) throws RemoteException;
    public boolean AddKhachHang(Tblkhachhang kh)throws RemoteException;
    public List<Tblchuyenbay> GetAllChuyenBay() throws RemoteException;
    public boolean AddChuyenBay(Tblchuyenbay cb) throws RemoteException;
    public boolean DeleteChuyenBay(String id) throws RemoteException;
}
