/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Controller;

import Server.Model.DBService;
import Server.Model.Tblkhachhang;
import Server.Model.Tblmaybay;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanh
 */
public class KhachHangController {

    public List<Tblkhachhang> GetAll() {
        List<Tblkhachhang> khachHangs = new ArrayList<Tblkhachhang>();

        try (
                Connection conn = DBService.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM tblkhachhang");) {
            while (rs.next()) {
                Tblkhachhang b = new Tblkhachhang();
                b.setSMaKH(rs.getString("sMaKH"));
                b.setSHoten(rs.getString("sHoten"));
                b.setITuoi(rs.getInt("iTuoi"));
                b.setSGT(rs.getString("sGT"));
                try {
                    b.setDNgaySinh(rs.getDate("dNgaySinh"));
                } catch (Exception ex) {

                }

                b.setICMND(rs.getInt("iCMND"));
                b.setSDC(rs.getString("sDC"));
                b.setSSDT(rs.getString("sSDT"));

                khachHangs.add(b);
            }
            return khachHangs;

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return khachHangs;
    }

    public boolean Add(Tblkhachhang kh) throws SQLException {

        String sql = "INSERT into tblkhachhang  (sMaKH,sHoten,iTuoi,sGT,dNgaySinh,iCMND,sDC,sSDT,) "
                + "VALUES (?, ?, ?)";
        ResultSet key = null;
        try (
                Connection conn = DBService.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            stmt.setString(1, kh.getSMaKH());
            stmt.setString(2, kh.getSHoten());
            stmt.setInt(3, kh.getITuoi());
            stmt.setString(4, kh.getSGT());
            try{
                stmt.setDate(5, (Date) kh.getDNgaySinh());
            }catch(Exception ex){
                
            }
            stmt.setInt(6, kh.getICMND());
            stmt.setString(7, kh.getSDC());
            stmt.setString(8, kh.getSSDT());


            int rowInserted = stmt.executeUpdate();

            if (rowInserted == 1) {
                key = stmt.getGeneratedKeys();
                key.next();
                int newKey = key.getInt(1);

            } else {
                System.err.println("No book inserted");
                return false;
            }
        }
        return true;

    }
}
