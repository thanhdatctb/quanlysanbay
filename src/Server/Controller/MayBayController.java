/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Controller;

import Server.Model.DBService;
import Server.Model.Tblmaybay;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author thanh
 */
public class MayBayController {

    //private EntityManager em = Persistence.createEntityManagerFactory("QuanLySanBayPU").createEntityManager();
    public List<Tblmaybay> GetAll() {
        List<Tblmaybay> maybays = new ArrayList<Tblmaybay>();
        String hql = String.format("select a from %s a", Tblmaybay.class.getName());
        System.out.println("HAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        try (
                Connection conn = DBService.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM tblmaybay");) {
            while (rs.next()) {
                Tblmaybay b = new Tblmaybay();
                b.setSHangHangKhong(rs.getString("sHangHangKhong"));
                b.setSMaMB(rs.getString("sMaMB"));
                b.setSTenMayBay(rs.getString("sTenMayBay"));
//                b.setId(rs.getInt("id")); //"id" is column name in table book
//                b.setTitle(rs.getString("title")); //"title" is column name in table book
//                b.setPages(rs.getInt("page")); //"page" is column name in table book
//                b.setAuthor(rs.getString("author")); //"author" is column name in table book
                maybays.add(b);
            }
            return maybays;

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return maybays;
    }
    public boolean Add(Tblmaybay mayBay) throws SQLException{
        
        String sql = "INSERT into tblmaybay  (`sMaMB`, `sTenMayBay`, `sHangHangKhong`) "
                + "VALUES (?, ?, ?)";
        ResultSet key = null;
        try (
            Connection conn = DBService.getConnection();
            PreparedStatement stmt = 
                    conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ) {
            stmt.setString(1, mayBay.getSMaMB());
            stmt.setString(2, mayBay.getSTenMayBay());
            stmt.setString(3, mayBay.getSHangHangKhong());
            
            int rowInserted = stmt.executeUpdate();
            
            if (rowInserted == 1) {
                key = stmt.getGeneratedKeys();
                key.next();
                int newKey = key.getInt(1);
                
            } else {
                System.err.println("No book inserted");
                return false;
            }
        }
        return true;
        
    }

}
