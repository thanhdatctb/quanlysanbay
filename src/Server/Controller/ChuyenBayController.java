/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.Controller;

import Server.Model.DBService;
import Server.Model.Tblchuyenbay;
import Server.Model.Tblmaybay;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanh
 */
public class ChuyenBayController {

    public List<Tblchuyenbay> GetAll() {
        List<Tblchuyenbay> maybays = new ArrayList<Tblchuyenbay>();
        String hql = String.format("select a from %s a", Tblmaybay.class.getName());
        System.out.println("HAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        try (
                Connection conn = DBService.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM tblchuyenbay");) {
            while (rs.next()) {
                Tblchuyenbay b = new Tblchuyenbay();

                b.setSMaCB(rs.getString("sMaCB"));
                b.setSTenCB(rs.getString("sTenCB"));
                try {
                    b.setDNgayKH(rs.getDate("dNgayKH"));
                } catch (Exception ex) {

                }

                b.setSGioBD(rs.getString("sGioBD"));
                b.setSGioKT(rs.getString("sGioKT"));
                b.setFMaxHL(rs.getDouble("fMaxHL"));
//                b.setId(rs.getInt("id")); //"id" is column name in table book
//                b.setTitle(rs.getString("title")); //"title" is column name in table book
//                b.setPages(rs.getInt("page")); //"page" is column name in table book
//                b.setAuthor(rs.getString("author")); //"author" is column name in table book
                maybays.add(b);
            }
            return maybays;

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return maybays;
    }

    public boolean Add(Tblchuyenbay mayBay) throws SQLException {

        String sql = "INSERT into tblchuyenbay(sMaCB,sTenCB,dNgayKH,sGioBD,sGioKT,sMaLT,sMaMB,fMaxHL) "
                + "VALUES (?, ?, ?, ?, ?, ?, ? , ?)";
        ResultSet key = null;
        try (
                Connection conn = DBService.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            stmt.setString(1, mayBay.getSMaCB());
            stmt.setString(2, mayBay.getSTenCB());
            try{
                stmt.setDate(3,(java.sql.Date) mayBay.getDNgayKH());
            }catch(Exception ex){
                stmt.setDate(3, null);
            }
            
            stmt.setString(4, mayBay.getSGioBD());
            stmt.setString(5, mayBay.getSGioKT());
            stmt.setString(6, mayBay.getSMaLT().getSMaLT());
            stmt.setString(7, mayBay.getSMaMB().getSMaMB());
            stmt.setDouble(8, mayBay.getFMaxHL());
            int rowInserted = stmt.executeUpdate();

            if (rowInserted == 1) {
                key = stmt.getGeneratedKeys();
                key.next();
                int newKey = key.getInt(1);

            } else {
                System.err.println("No book inserted");
                return false;
            }
        }
        return true;

    }
    public boolean Delete(String id) throws SQLException{
        
        String sql = "delete from tblchuyenbay where sMaCB = ?";
         Connection conn = DBService.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1,id);
        stmt.executeUpdate();
        
        return true;
    }
}
