/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Server.Controller.AdminController;
import Server.Controller.ChuyenBayController;
import Server.Controller.KhachHangController;
import Server.Controller.MayBayController;
import Server.Model.Tblchuyenbay;
import Server.Model.Tblkhachhang;
import Server.Model.Tblmaybay;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanh
 */
public class QuanLySanBayImplement extends UnicastRemoteObject implements IQuanLySanBay{
    private MayBayController mayBayController = new MayBayController();
    private KhachHangController khachHangController = new KhachHangController();
    private AdminController adminController = new AdminController();
    private ChuyenBayController chuyenBayController = new ChuyenBayController();
    public QuanLySanBayImplement() throws RemoteException{
        super();
    }
    

    @Override
    public boolean LogIn(String username, String password) throws RemoteException {
        try {
            return adminController.LogIn(username, password);
        } catch (SQLException ex) {
            Logger.getLogger(QuanLySanBayImplement.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return false;
    }

    @Override
    public List<Tblmaybay> GetAllMayBay() throws RemoteException {
        return mayBayController.GetAll();
    }

    @Override
    public List<Tblkhachhang> GetAllKhachHang() throws RemoteException {
        return khachHangController.GetAll();
    }

    @Override
    public boolean AddMayBay(Tblmaybay mb) throws RemoteException {
        try {
            return mayBayController.Add(mb);
        } catch (SQLException ex) {
            Logger.getLogger(QuanLySanBayImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean AddKhachHang(Tblkhachhang kh) throws RemoteException {
        try {
            return khachHangController.Add(kh);
        } catch (SQLException ex) {
            Logger.getLogger(QuanLySanBayImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Tblchuyenbay> GetAllChuyenBay() throws RemoteException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return chuyenBayController.GetAll();
    }

    @Override
    public boolean AddChuyenBay(Tblchuyenbay cb) throws RemoteException {
        try {
            return chuyenBayController.Add(cb);
        } catch (SQLException ex) {
            Logger.getLogger(QuanLySanBayImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean DeleteChuyenBay(String id) throws RemoteException {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            return chuyenBayController.Delete(id);
        } catch (SQLException ex) {
            Logger.getLogger(QuanLySanBayImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    

    
}
