-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 03, 2020 lúc 04:09 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `btl_quanlysanbay`
--

DELIMITER $$
--
-- Thủ tục
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pr_DSKH_LTTheoDiemDen` (`p_diemden` NVARCHAR(30))  BEGIN
      -- SQLINES DEMO *** OR EVALUATION USE ONLY
      -- SQLINES LICENSE FOR EVALUATION USE ONLY
      SELECT  tblKhachHang.sMaKH, tblKhachHang.sHoten, tblLoTrinh.sDiemKT
	  FROM tblKhachHang, tblKH_CB, tblLoTrinh, tblChuyenbay
	  WHERE tblKhachHang.sMaKH = tblKH_CB.sMaKH
	        AND tblChuyenbay.sMaCB = tblKH_CB.sMaCB
			AND tblChuyenbay.sMaLT = tblLoTrinh.sMaLT
			AND tblLoTrinh.sDiemKT = p_diemden;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pr_DSNVTheoHsl` (`p_hsl` DOUBLE)  BEGIN
      -- SQLINES DEMO *** OR EVALUATION USE ONLY
      -- SQLINES LICENSE FOR EVALUATION USE ONLY
      SELECT * FROM tblNhanVien
	  WHERE sHsl = p_hsl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pr_DSNVTheoViTri` (`p_vt` NVARCHAR(30))  BEGIN
      -- SQLINES DEMO *** OR EVALUATION USE ONLY
      -- SQLINES LICENSE FOR EVALUATION USE ONLY
      SELECT * FROM tblNhanVien
	  WHERE sVitri = p_vt;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'admin1', 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblchuyenbay`
--

CREATE TABLE `tblchuyenbay` (
  `sMaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sTenCB` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `dNgayKH` datetime(3) DEFAULT NULL,
  `sGioBD` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `sGioKT` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `sMaLT` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `sMaMB` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `fMaxHL` double DEFAULT 7
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblchuyenbay`
--

INSERT INTO `tblchuyenbay` (`sMaCB`, `sTenCB`, `dNgayKH`, `sGioBD`, `sGioKT`, `sMaLT`, `sMaMB`, `fMaxHL`) VALUES
('1', 'Hà Nội - Đà Nẵng', '0000-00-00 00:00:00.000', '8 giờ 30 phút', '11 giờ 00 phút ', 'T1', 'X1', 7),
('2', 'Đà Nẵng - TP.Hồ Chí Minh', '0000-00-00 00:00:00.000', '21 giờ 00 phút', '23 giờ 30 phút', 'T2', 'X2', 7),
('3', 'Hà Nội - Nha Trang', '0000-00-00 00:00:00.000', '22 giờ 00 phút', '3 giờ 00 phút', 'T3', 'X3', 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblkhachhang`
--

CREATE TABLE `tblkhachhang` (
  `sMaKH` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sHoten` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `iTuoi` int(11) DEFAULT NULL,
  `sGT` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `dNgaySinh` datetime(3) DEFAULT NULL,
  `iCMND` int(11) DEFAULT NULL,
  `sDC` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sSDT` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblkhachhang`
--

INSERT INTO `tblkhachhang` (`sMaKH`, `sHoten`, `iTuoi`, `sGT`, `dNgaySinh`, `iCMND`, `sDC`, `sSDT`) VALUES
('A1', 'Đỗ Đức Tiến', 20, 'Nam', '0000-00-00 00:00:00.000', 12345, 'Bắc Quang - Hà Giang', '0397192888'),
('A2', 'Đỗ Thị Mai', 21, 'Nu', '0000-00-00 00:00:00.000', 12346, 'Đông Anh - Hà Nội', '0395123555'),
('A3', 'Nguyễn Thế Việt', 22, 'Nam', '0000-00-00 00:00:00.000', 12347, 'Thuận Thành - Bắc Ninh', '0398125444'),
('A4', 'Nguyễn Thị Hường', 29, 'Nu', '0000-00-00 00:00:00.000', 12348, 'Lâm Thao - Phú Thọ', '0397158666'),
('A5', 'Lưu Minh Tú', 23, 'Nam', '0000-00-00 00:00:00.000', 12349, 'Yên Sơn - Tuyên Quang', '0396123555');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblkh_cb`
--

CREATE TABLE `tblkh_cb` (
  `sMaKH` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sMaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `fBuyHL` double DEFAULT NULL,
  `sLoaiVe` varchar(10) CHARACTER SET utf8 DEFAULT NULL CHECK (`sLoaiVe` in ('Thường','Thương Gia')),
  `fGiaVe` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblkh_cb`
--

INSERT INTO `tblkh_cb` (`sMaKH`, `sMaCB`, `fBuyHL`, `sLoaiVe`, `fGiaVe`) VALUES
('A1', '1', 3, 'Thường', 1000000),
('A2', '2', 5, 'Thương Gia', 2000000),
('A3', '3', 2, 'Thường', 900000),
('A4', '1', 7, 'Thương Gia', 2500000),
('A5', '2', 3, 'Thường', 1000000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbllotrinh`
--

CREATE TABLE `tbllotrinh` (
  `sMaLT` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sDiemBD` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sDiemKT` varchar(30) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbllotrinh`
--

INSERT INTO `tbllotrinh` (`sMaLT`, `sDiemBD`, `sDiemKT`) VALUES
('T1', 'Nội Bài', 'Đà Nẵng'),
('T2', 'Đà Nẵng', 'Tân Sơn Nhất'),
('T3', 'Nội Bài', 'Cam Ranh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblmaybay`
--

CREATE TABLE `tblmaybay` (
  `sMaMB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sTenMayBay` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sHangHangKhong` varchar(30) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblmaybay`
--

INSERT INTO `tblmaybay` (`sMaMB`, `sTenMayBay`, `sHangHangKhong`) VALUES
('ad', 'fdsa', 'dfsa'),
('adjjj', 'fdsa', 'dfsa'),
('dsfdsaf', 'adfadsfadsfasdfadsfa', 'dfadfa'),
('ed', 'ưe', 'ew'),
('X1', 'Airbus A321NEO', 'Bamboo Airways'),
('X2', 'Boeing 747', 'VietNam Airlines'),
('X3', 'Boeing 787', 'Vietjet Air');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblnhanvien`
--

CREATE TABLE `tblnhanvien` (
  `sMaNV` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sHoten` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sGT` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `sDC` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sVitri` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sHsl` double DEFAULT NULL,
  `tuoi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblnhanvien`
--

INSERT INTO `tblnhanvien` (`sMaNV`, `sHoten`, `sGT`, `sDC`, `sVitri`, `sHsl`, `tuoi`) VALUES
('L1', 'Trần Thế Long', 'Nam', 'Hoàng Mai - Hà Nội', 'Phi Công Chính', 2.5, 25),
('L2', 'Vũ Huy Hải', 'Nam', 'Thanh Xuân - Hà Nội', 'Kỹ thuật viên', 2.1, 23),
('L3', 'Trương Minh Tuân', 'Nam', 'Hai Bà Trưng - Hà Nội', 'Phi Công Phụ', 2.3, 28),
('L4', 'Ngô Minh Hiếu', 'Nam', 'Hoàn Kiếm - Hà Nội', 'Nhân Viên An Ninh', 2, 29),
('L5', 'Lưu Thùy Chi', 'Nu', 'Ba Đình - Hà Nội', 'Tiếp Viên Hàng Không', 2.2, 24);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblnv_cb`
--

CREATE TABLE `tblnv_cb` (
  `sMaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sMaNV` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblnv_cb`
--

INSERT INTO `tblnv_cb` (`sMaCB`, `sMaNV`) VALUES
('1', 'L2'),
('2', 'L4'),
('3', 'L5');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblnv_sdt`
--

CREATE TABLE `tblnv_sdt` (
  `sMaNV` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sSDT` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblnv_sdt`
--

INSERT INTO `tblnv_sdt` (`sMaNV`, `sSDT`) VALUES
('L1', '0358123696'),
('L2', '0395123444'),
('L3', '0397123584'),
('L4', '0392789456'),
('L5', '0394123789');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tblchuyenbay`
--
ALTER TABLE `tblchuyenbay`
  ADD PRIMARY KEY (`sMaCB`),
  ADD KEY `FK_CB_LT` (`sMaLT`),
  ADD KEY `FK_CB_MB` (`sMaMB`);

--
-- Chỉ mục cho bảng `tblkhachhang`
--
ALTER TABLE `tblkhachhang`
  ADD PRIMARY KEY (`sMaKH`),
  ADD UNIQUE KEY `iCMND` (`iCMND`);

--
-- Chỉ mục cho bảng `tblkh_cb`
--
ALTER TABLE `tblkh_cb`
  ADD PRIMARY KEY (`sMaKH`,`sMaCB`),
  ADD KEY `FK_tblKH_CB1` (`sMaCB`);

--
-- Chỉ mục cho bảng `tbllotrinh`
--
ALTER TABLE `tbllotrinh`
  ADD PRIMARY KEY (`sMaLT`);

--
-- Chỉ mục cho bảng `tblmaybay`
--
ALTER TABLE `tblmaybay`
  ADD PRIMARY KEY (`sMaMB`);

--
-- Chỉ mục cho bảng `tblnhanvien`
--
ALTER TABLE `tblnhanvien`
  ADD PRIMARY KEY (`sMaNV`);

--
-- Chỉ mục cho bảng `tblnv_cb`
--
ALTER TABLE `tblnv_cb`
  ADD PRIMARY KEY (`sMaNV`,`sMaCB`),
  ADD KEY `FK_tblNV_CB` (`sMaCB`);

--
-- Chỉ mục cho bảng `tblnv_sdt`
--
ALTER TABLE `tblnv_sdt`
  ADD PRIMARY KEY (`sMaNV`,`sSDT`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `tblchuyenbay`
--
ALTER TABLE `tblchuyenbay`
  ADD CONSTRAINT `FK_CB_LT` FOREIGN KEY (`sMaLT`) REFERENCES `tbllotrinh` (`sMaLT`),
  ADD CONSTRAINT `FK_CB_MB` FOREIGN KEY (`sMaMB`) REFERENCES `tblmaybay` (`sMaMB`);

--
-- Các ràng buộc cho bảng `tblkh_cb`
--
ALTER TABLE `tblkh_cb`
  ADD CONSTRAINT `FK_tblKH_CB` FOREIGN KEY (`sMaKH`) REFERENCES `tblkhachhang` (`sMaKH`),
  ADD CONSTRAINT `FK_tblKH_CB1` FOREIGN KEY (`sMaCB`) REFERENCES `tblchuyenbay` (`sMaCB`);

--
-- Các ràng buộc cho bảng `tblnv_cb`
--
ALTER TABLE `tblnv_cb`
  ADD CONSTRAINT `FK_tblNV_CB` FOREIGN KEY (`sMaCB`) REFERENCES `tblchuyenbay` (`sMaCB`),
  ADD CONSTRAINT `FK_tblNV_CB1` FOREIGN KEY (`sMaNV`) REFERENCES `tblnhanvien` (`sMaNV`);

--
-- Các ràng buộc cho bảng `tblnv_sdt`
--
ALTER TABLE `tblnv_sdt`
  ADD CONSTRAINT `FK_tblNV_SDT` FOREIGN KEY (`sMaNV`) REFERENCES `tblnhanvien` (`sMaNV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
