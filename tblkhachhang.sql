-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 03, 2020 lúc 04:06 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `btl_quanlysanbay`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblkhachhang`
--

CREATE TABLE `tblkhachhang` (
  `sMaKH` varchar(10) CHARACTER SET utf8 NOT NULL,
  `sHoten` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `iTuoi` int(11) DEFAULT NULL,
  `sGT` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `dNgaySinh` datetime(3) DEFAULT NULL,
  `iCMND` int(11) DEFAULT NULL,
  `sDC` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `sSDT` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblkhachhang`
--

INSERT INTO `tblkhachhang` (`sMaKH`, `sHoten`, `iTuoi`, `sGT`, `dNgaySinh`, `iCMND`, `sDC`, `sSDT`) VALUES
('A1', 'Đỗ Đức Tiến', 20, 'Nam', '0000-00-00 00:00:00.000', 12345, 'Bắc Quang - Hà Giang', '0397192888'),
('A2', 'Đỗ Thị Mai', 21, 'Nu', '0000-00-00 00:00:00.000', 12346, 'Đông Anh - Hà Nội', '0395123555'),
('A3', 'Nguyễn Thế Việt', 22, 'Nam', '0000-00-00 00:00:00.000', 12347, 'Thuận Thành - Bắc Ninh', '0398125444'),
('A4', 'Nguyễn Thị Hường', 29, 'Nu', '0000-00-00 00:00:00.000', 12348, 'Lâm Thao - Phú Thọ', '0397158666'),
('A5', 'Lưu Minh Tú', 23, 'Nam', '0000-00-00 00:00:00.000', 12349, 'Yên Sơn - Tuyên Quang', '0396123555');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tblkhachhang`
--
ALTER TABLE `tblkhachhang`
  ADD PRIMARY KEY (`sMaKH`),
  ADD UNIQUE KEY `iCMND` (`iCMND`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
